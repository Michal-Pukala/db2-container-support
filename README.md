####################################################

db2 multicontainer

Author Michal Pukala, Jakub Bak

####################################################

Clone git repository
git clone https://gitlab.com/Michal-Pukala/db2-container-support.git

--- FEATURES ---
### 1. TSA+HADR two containers deployment ###
REQUIREMENTS:
Db2 Versions: 11.1.4.5 - 11.5.7
Copy *tar.gz Db2 setup package into git cloned directory
cp <*tar.gz> <git_directory>/db2-container-support/setup_package

Two containers will be created as Primary and Standby with TSA enabled.
DEPLOY:
<git_directory>/db2-container-support/deploy/db2_multicontainer.sh tsa_remote
- Monitor deployment process in log file: /tmp/db2contout/logs/db2multicontainer*

LOGS of deployment:
On host machine: /tmp/db2contout/logs/db2multicontainer*

### 2.HADR two containers deployment ###
REQUIREMENTS:
Db2 Versions: 9.7 - 11.5.7
Copy *tar.gz Db2 setup package into git cloned directory
cp <*tar.gz> <git_directory>/db2-container-support/setup_package

Two containers will be created as Primary and Standby.
DEPLOY:
<git_directory>/db2-container-support/deploy/db2_multicontainer.sh hadr_remote
- Monitor deployment process in log file: /tmp/db2contout/logs/db2multicontainer*

LOGS of deployment:
On host machine: /tmp/db2contout/logs/db2multicontainer*

### 3. HADR local in one container ###
REQUIREMENTS:
Db2 Versions: 9.7 - 11.5.7
Copy *tar.gz Db2 setup package into git cloned directory
cp <*tar.gz> <git_directory>/db2-container-support/setup_package

One container will be created with two instances as Primary and Standby
DEPLOY:
<git_directory>/db2-container-support/deploy/db2_multicontainer.sh hadr_local
- Monitor deployment process in log file: /tmp/db2contout/logs/db2multicontainer*

LOGS of deployment:
On host machine: /tmp/db2contout/logs/db2multicontainer*

### 4. Default instance creation in one container ###
REQUIREMENTS:
Db2 Versions: 9.7 - 11.5.7
Copy *tar.gz Db2 setup package into git cloned directory
cp <*tar.gz> <git_directory>/db2-container-support/setup_package

One container with default instance will be created
DEPLOY:
<git_directory>/db2-container-support/deploy/db2_multicontainer.sh default_instance
- Monitor deployment process in log file: /tmp/db2contout/logs/db2multicontainer*

LOGS of deployment:
On host machine: /tmp/db2contout/logs/db2multicontainer*

REMOVE/CLEANUP:
Stop container
docker stop <container_name> <container_name2>
Remove container
docker rm <container_name> <container_name2>
Remove built image
docker rmi -f <image_name>
