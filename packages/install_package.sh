#!/bin/bash

#Define input list file of packages
INPUT=$1
LACK_P="package_out.out"
i=$(wc -l $INPUT | awk '{print $1}')

#Clean LACK_P if exists
echo "" > $LACK_P
for (( x=1; x<=$i; x++ ))
do
        p_name=$(sed "${x}q;d" $INPUT)
        printf "\nInstalling $p_name ...\n"
        apt install -y $p_name 2> /tmp/ip_rep.out
                #Check if package can be find
                str1=$(cat /tmp/ip_rep.out | grep 'Unable to locate package')
                if [ "$str1" == "E: Unable to locate package $p_name" ]; then
                        printf "$p_name Not Existing in repository"
                        printf "$p_name\n" >> $LACK_P
                else
                        printf "\n $p_name Installed"
                fi
        #printf "\n$p_name INSTALLED\n\n"
        count=$(expr $i - $x)
        printf "\n\nPackages left: $count"
        wait
done
