FROM quay.io/pukalam12/db2_base:v1.2

RUN mkdir -p /opt/db2_setup/setup_package && mkdir /opt/db2_setup/scripts && mkdir /opt/db2_setup/hadr && mkdir /opt/db2_setup/logs

COPY ./setup_package/ /opt/db2_setup/setup_package/
COPY ./scripts/ /opt/db2_setup/scripts

ENTRYPOINT ["/opt/db2_setup/scripts/entrypoint.sh"]
