#!/bin/bash

#Variables
HADR_USER1="db2hadrp"
HADR_USER2="db2hadrs"
DB_NAME="HLDB"
HOSTNAME_LOCAL=$(hostname)
HOSTNAME_LOCAL_IP=$(grep $HOSTNAME_LOCAL /etc/hosts | awk '{print $1}')
PORT_PRIM=50100
PORT_STAND=50101
SS="/opt/db2_setup/hadr/STANDBY_STARTED"
PC="/opt/db2_setup/hadr/PRIMARY_CONFIGURED"
SC="/opt/db2_setup/hadr/STANDBY_CONFIGURED"
SSTARTED="/opt/db2_setup/hadr/STANDBY_STARTED_START"
TIMESTAMP=$(date +%F_%T)
IP_QUORUM=$(route | awk '/default/ { print $2 }')
#Logging
LOG_FILE=$(ls -t /opt/db2_setup/logs/db2multicontainer* | head -1)
loginfo() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] INFO: $1" >> $LOG_FILE
}

logrc() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] ERROR: $1" >> $LOG_FILE
}

#Create Users function
add_user() {
  loginfo "Adding user $1..."
	useradd -r -g db2iadm1 -m -d /home/$1 $1
	usermod --shell /bin/bash $1
	loginfo "User $1 created!"
}

#Create instance
instance_create() {
	loginfo "Creating instance $1..."
  /opt/ibm/db2/$VERSION/instance/db2icrt -u db2fenc1 $1
  wait
	if [ ! $(sudo -u $HADR_USER1 -i  bash -c  "db2ilist | grep -q $HADR_USER1") ]
		then
   		loginfo "Instance $1 created!"
		else
    	logrc "Instance $HADR_USER1 not exists!"
			exit 0
	fi
}

#create database on PRIMARY
database_create() {
	sudo -u $1 -i  bash -c 'db2start'
	wait
  loginfo "Database $2 creating..."
	sudo -u $1 -i bash -c "db2 'create db $2'"
	wait
	if [ ! $(sudo -u $HADR_USER1 -i  bash -c "db2 list db directory | grep -q $DB_NAME") ]
  	then
    	loginfo "Database $DB_NAME created!"
    else
    	logrc "Database $DB_NAME not exisits!"
      exit 0
  fi

	sudo -u $1 -i bash -c "db2 'UPDATE DB CFG FOR $DB_NAME USING LOGINDEXBUILD ON LOGARCHMETH1 LOGRETAIN'"
	if [ ! $(sudo -u $HADR_USER1 -i  bash -c  "db2 get db cfg for $DB_NAME | grep -q LOGRETAIN") ]
  	then
    	loginfo "LOGRETAIN set"
    else
    	logrc "Db cfg LOGRETAIN not set!"
      exit 0
  fi
}

#hadr primary backup
backup_primary () {
  loginfo "Starting Primary database backup..."
	sudo -u $1 -i bash -c "mkdir /home/$1/backup"
	sudo -u $1 -i bash -c "db2 'BACKUP DB $DB_NAME TO /home/$1/backup'"
	wait
	if [ ! $(ls -lt /home/$1/backup/ | grep -q $DB_NAME) ]
  	then
    	mv /home/$1/backup/$DB_NAME* /opt/db2_setup/hadr/
      loginfo "Primary database backup complete"
      touch /opt/db2_setup/hadr/PRIMARY_BACKUP
    else
    	logrc "Primary backup not exists!"
      exit 0
  fi
}

#IP_sharing
ip_sharing(){
	grep $HOSTNAME_LOCAL  /etc/hosts >> /opt/db2_setup/hadr/ip_primary.out
}

#Grab IP from Standby
standby_ip_gather(){
	cat  /opt/db2_setup/hadr/ip_standby.out >> /etc/hosts
 	HOSTNAME_REMOTE=$(cat /opt/db2_setup/hadr/host_standby.out)
	HOSTNAME_REMOTE_IP=$(grep $HOSTNAME_REMOTE /etc/hosts | awk '{print $1}')
}
#Waiting for standby init
standby_init() {
	loginfo "Primary waiting for Standby init..."
	while [ ! -e $SC ]
	do
		sleep 5
	done
}

#Standby configuration
database_config_primary(){
	sudo -u $1 -i bash -c "db2 'UPDATE DB CFG FOR $DB_NAME USING HADR_LOCAL_HOST $HOSTNAME_LOCAL_IP HADR_LOCAL_SVC $PORT_PRIM HADR_SYNCMODE SYNC HADR_REMOTE_HOST $HOSTNAME_REMOTE_IP HADR_REMOTE_SVC $PORT_STAND HADR_REMOTE_INST $HADR_USER2 hadr_peer_window 120'"
	wait
	if [ ! $(sudo -u $1 -i bash -c "db2 get db cfg for $DB_NAME | grep -q $HADR_USER2") ]
  	then
    	loginfo "Database full config set"
		  touch $PC
		else
    	logrc "Database full config not set!"
      exit 0
  fi
}

#Waiting for standby init
standby_start_state() {
	loginfo "Primary wating for Standby start..."
	while [ ! -e $SSTARTED ]
		do
  		sleep 5
		done
}

database_start_primary(){
	loginfo "HADR starting on Primary..."
  sudo -u $HADR_USER1 -i bash -c "db2start"
  wait
  sudo -u $HADR_USER1 -i bash -c "db2 start hadr on db $DB_NAME as primary"
  wait
	if [ ! $(sudo -u $1 -i bash -c "db2pd -db $DB_NAME -hadr | grep -q CONNECTED") ]
  	then
    	loginfo "HADR Primary started"
    else
    	logrc "HADR Primary not started!"
      exit 0
  fi
}

setup_envoirnment(){
	loginfo "Starting TSA setup on Primary"
  #enable watchdog process
	#Remove after image container update
	sudo apt-get update
	wait
	sudo apt-get install linux-modules-5.4.0-126-generic
	wait
	sudo apt-get install linux-modules-5.4.0-128-generic
	wait
	##
  modprobe softdog
  #install SAM packages process
 	if [ ! $(sudo -u root -i bash -c "$(find /opt/db2_setup/setup_package/ -name 'installSAM') --noliccheck --silent -l /tmp/installSAM.log") ]
  	then
    	loginfo "SAM installed succesfully "
    else
    	logrc "installSAM failed"
      exit 0
  fi
  #install HADR monitor scripts
  $(find /opt -name 'db2cptsa')
  if [ !$(find /opt -name 'db2cptsa' | grep -q DBI1110I) ]
  	then
  		loginfo "TSA scripts installed "
  	else
  		logrc "TSA scripts install  failed"
    	exit 0
  fi

  export CT_MANAGEMENT_SCOPE=2
	#need stop process srcmsrt
  srcmstrctrl -k
  systemctl start srcmstr
  systemctl start ctrmc
  systemctl enable ctrmc
  systemctl enable srcmstr
  if [ !$(systemctl status ctrmc | grep -q running) ]
  	then
    	loginfo "ctrmc service started "
    else
    	logrc "ctrmc service disabled"
      exit 0
  fi

	HOSTNAME_REMOTE=$(cat /opt/db2_setup/hadr/host_standby.out)
	preprpnode $HOSTNAME_LOCAL $HOSTNAME_REMOTE
	if [ !$(grep -q "$HOSTNAME_LOCAL" /var/ct/cfg/ctrmc.acls) ] && [ !$(grep -q "$HOSTNAME_REMOTE" /var/ct/cfg/ctrmc.acls) ]
  	then
    	loginfo "preprnode executed succesfull "
    else
    	logrc "preprnode failed"
      exit 0
  fi
}

db2haicu_setup(){
	HOSTNAME_REMOTE_IP=$(grep $HOSTNAME_REMOTE /etc/hosts | awk '{print $1}')
echo "
<!-- =============================================-->
<!-- =           DB2 Domain Information         = -->
<!-- =   Quorum Device, Network Interface and   = -->
<!-- =          Cluster Node Details            = -->
<!-- =============================================-->

<DB2Cluster xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"db2ha.xsd\" version=\"1.0\" clusterManagerName=\"TSA\">
<ClusterDomain domainName=\"hahadr\">
<Quorum quorumDeviceProtocol=\"network\" quorumDeviceName=\"$IP_QUORUM\"/>
<PhysicalNetwork physicalNetworkName=\"db2_private_network_0\" physicalNetworkProtocol=\"ip\">
<Interface interfaceName=\"eth0\" clusterNodeName=\"$HOSTNAME_REMOTE\">
<IPAddress baseAddress=\"$HOSTNAME_REMOTE_IP\" subnetMask=\"255.255.0.0\" networkName=\"db2_private_network_0\"/>
</Interface>
<Interface interfaceName=\"eth0\" clusterNodeName=\"$HOSTNAME_LOCAL\">
<IPAddress baseAddress=\"$HOSTNAME_LOCAL_IP\" subnetMask=\"255.255.0.0\" networkName=\"db2_private_network_0\"/>
</Interface>
</PhysicalNetwork>
<ClusterNode clusterNodeName=\"$HOSTNAME_REMOTE\"/>
<ClusterNode clusterNodeName=\"$HOSTNAME_LOCAL\"/>
</ClusterDomain>

<!-- =============================================-->
<!-- =             Failover Policy              = -->
<!-- =============================================-->

<FailoverPolicy>
<HADRFailover/>
</FailoverPolicy>

<!-- =============================================-->
<!-- =             DB Partition Set             = -->
<!-- =============================================-->

<DB2PartitionSet>
<DB2Partition dbpartitionnum=\"0\" instanceName=\"db2hadrp\"/>
</DB2PartitionSet>

<!-- =============================================-->
<!-- =          HADR Database Details           = -->
<!-- =============================================-->

<HADRDBSet>
<HADRDB databaseName=\"HLDB\" localInstance=\"db2hadrp\" remoteInstance=\"db2hadrs\" localHost=\"$HOSTNAME_LOCAL\" remoteHost=\"$HOSTNAME_REMOTE\" monitorDataMounts=\"false\"/>
</HADRDBSet>
</DB2Cluster>
" >> /opt/db2_setup/hadr/haicu_primary.xml

	sudo -u $HADR_USER1 -i bash -c "db2haicu -f /opt/db2_setup/hadr/haicu_primary.xml"
  if [ !$(lssam | grep -q IBM) ]
 	then
		loginfo "db2haicu installed on Primary"
  else
		logrc "db2haicu installation failed"
		exit 0
  fi
}

standby_tsa_start() {
	loginfo "Primary waiting for Standby TSA setup..."
	STSASTART="/opt/db2_setup/hadr/tsa_standby_setup_done"
	while [ ! -e $STSASTART ]
		do
  		sleep 5
		done
}

envoirnment_cleanup(){
	rm /opt/db2_setup/hadr/*
	loginfo "Cleanup done"
}

#Primary hostname
echo $HOSTNAME > '/opt/db2_setup/hadr/host_primary.out'
ip_sharing
add_user $HADR_USER1
instance_create $HADR_USER1
database_create $HADR_USER1 $DB_NAME
backup_primary $HADR_USER1
standby_init
standby_ip_gather
setup_envoirnment
database_config_primary $HADR_USER1
standby_start_state
database_start_primary
standby_tsa_start
db2haicu_setup
envoirnment_cleanup
