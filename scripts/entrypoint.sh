#!/bin/bash

CONTAINER_ALREADY_STARTED="CONTAINER_ALREADY_STARTED_PLACEHOLDER"
if [ ! -e $CONTAINER_ALREADY_STARTED ]; then
    touch $CONTAINER_ALREADY_STARTED
    echo "-- First container startup --"
    # YOUR_JUST_ONCE_LOGIC_HERE
    #bash /opt/db2_setup/scripts/setup_db2.sh -> if no docker.exec in deploy script please uncomment this line 
    exec /sbin/init
else
    echo "-- Not first container startup --"
    systemctl start srcmstr
    systemctl start ctrmc
    /bin/bash
fi
