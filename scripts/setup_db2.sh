#!/bin/bash

#####################################################
######		Author: Michal Pukala
######		db2_setup in container
#####
####################################################

#Includes
SCRIPT_DI="/opt/db2_setup/scripts/default_instance.sh"
SCRIPT_HL="/opt/db2_setup/scripts/hadr_local.sh"
SCRIPT_STANDBY="/opt/db2_setup/scripts/hadr_standby.sh"
SCRIPT_PRIMARY="/opt/db2_setup/scripts/hadr_primary.sh"
SCRIPT_TSA_PRIMARY="/opt/db2_setup/scripts/hadr_primary_tsa.sh"
SCRIPT_TSA_STANDBY="/opt/db2_setup/scripts/hadr_standby_tsa.sh"

#Variables
DIR=/opt/db2_setup/setup_package
VERSION=$(find $DIR -maxdepth 1 -name "v*" | cut -f5 -d"/" | cut -f1 -d"_" | cut -f1-2 -d".")
SETUP_DIR=/opt/ibm/db2/$VERSION
CHECK_VER=$(echo $VERSION | cut -f1 -d"f" | cut -f1 -d"_")
TIMESTAMP=$(date +%F_%T)
HOSTNAME_LOCAL=$(hostname)

#Logging
LOG_FILE=$(ls -t /opt/db2_setup/logs/db2multicontainer* | head -1)
loginfo() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] INFO: $1" >> $LOG_FILE
}

logrc() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] ERROR: $1" >> $LOG_FILE
}

#Create Users
#Adding default groups
groupadd db2iadm1
groupadd db2fsdm1
groupadd dasadm1
loginfo "Default groups created"

#Adding default users
useradd -r -g db2iadm1 -m -d /home/db2inst1 db2inst1
usermod --shell /bin/bash db2inst1
useradd -g db2fsdm1 -m -d /home/db2fenc1 db2fenc1
useradd -g dasadm1 -m -d /home/dasusr1 dasusr1
mkdir /home/db2inst1/support_tools
mkdir /home/db2inst1/outputs
loginfo "Default users added"

#Add authorizations
chown -R db2inst1:db2iadm1 /home/db2inst1/
chmod -R 755 /home/db2inst1/
chmod 755 -R /home/db2inst1/support_tools
chown -R db2inst1:db2iadm1 /home/db2inst1/support_tools
chmod -R 755 /home/db2inst1/outputs
chown -R db2inst1:db2iadm1 /home/db2inst1/outputs
loginfo "Authorizations modified"

#Unpack setup package
loginfo "Unpacking Db2 package..."
#Unpack from default dir
tar -zxf $DIR/v* --directory $DIR > /dev/null
loginfo "Db2 package unpacked"


#Setup db2 binaries
#Set db2_install path
DB2_IN_PATH=$(find $DIR -maxdepth 1 \( -name "server*" -o -name  "universal*" \))
#Check version
if [ $CHECK_VER == 'v11.5' ]
then
loginfo "Db2 $VERSION installation starting..."
$DB2_IN_PATH/db2_install -n -y -b /opt/ibm/db2/$VERSION -p SERVER
wait
elif [ $CHECK_VER == 'v11.1' ]
then
loginfo "Db2 $VERSION installation starting..."
$DB2_IN_PATH/db2_install -n -y -b /opt/ibm/db2/$VERSION -p SERVER
wait
elif [ $CHECK_VER == 'v10.5' ]
then
loginfo "Db2 $VERSION installation starting..."
$DB2_IN_PATH/db2_install -n -b /opt/ibm/db2/$VERSION -p SERVER
wait
else
loginfo "Db2 $VERSION installation starting..."
$DB2_IN_PATH/db2_install -n -b /opt/ibm/db2/$VERSION -p AESE
wait
fi
loginfo "Db2 $VERSION installation Complete!"

#Creating default instance
export VERSION

#Default instance
if [ $DEFAULT_INSTANCE == 'YES' ]
then
	loginfo "Default instance setup starting..."
	bash "$SCRIPT_DI"
wait
fi

#HADR local setup
if [ $HADR_LOCAL == 'YES' ]
then
	loginfo "HADR local deployment starting..."
bash "$SCRIPT_HL"
wait
fi

#HADR STANDBY setup
if [ $HADR_STANDBY == 'YES' ]
then
				loginfo "HADR STANDBY deployment starting..."
bash "$SCRIPT_STANDBY"
wait
fi

#HADR PRIMARY setup
if [ $HADR_PRIMARY == 'YES' ]
then
				loginfo "HADR PRIMARY deployment starting..."
bash "$SCRIPT_PRIMARY"
wait
fi

#HADR STANDBY setup
if [ $HADR_STANDBY_TSA == 'YES' ]
then
				loginfo "HADR STANDBY TSA deployment starting..."
bash "$SCRIPT_TSA_STANDBY"
wait
fi

#HADR PRIMARY setup
if [ $HADR_PRIMARY_TSA == 'YES' ]
then
				loginfo "HADR PRIMARY TSA deployment starting..."
bash "$SCRIPT_TSA_PRIMARY"
wait
fi
