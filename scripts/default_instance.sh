#!/bin/bash
HOSTNAME_LOCAL=$(hostname)

#Logging
TIMESTAMP=$(date +%F_%T)
LOG_FILE=$(ls -t /opt/db2_setup/logs/db2multicontainer* | head -1)
loginfo() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] INFO: $1" >> $LOG_FILE
}

logrc() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] ERROR: $1" >> $LOG_FILE
}

#Create instance
loginfo "Creating default instance db2inst1..."
/opt/ibm/db2/$VERSION/instance/db2icrt -u db2fenc1 db2inst1
wait
if [ ! $(sudo -u db2inst1 -i  bash -c  "db2ilist | grep -q db2inst1") ]
then
  loginfo "Instance db2inst1 created!"
else
  logrc "Instance db2inst1 not exists!"
  exit 0
fi
