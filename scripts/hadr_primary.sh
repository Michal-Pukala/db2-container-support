#!/bin/bash

#Variables
HADR_USER1="db2hadrp"
HADR_USER2="db2hadrs"
DB_NAME="HLDB"
HOSTNAME_LOCAL=$(hostname)
HOSTNAME_LOCAL_IP=$(grep $HOSTNAME_LOCAL /etc/hosts | awk '{print $1}')
PORT_PRIM=50100
PORT_STAND=50101
SS="/opt/db2_setup/hadr/STANDBY_STARTED"
PC="/opt/db2_setup/hadr/PRIMARY_CONFIGURED"
SC="/opt/db2_setup/hadr/STANDBY_CONFIGURED"
SSTARTED="/opt/db2_setup/hadr/STANDBY_STARTED_START"
TIMESTAMP=$(date +%F_%T)

#Logging
LOG_FILE=$(ls -t /opt/db2_setup/logs/db2multicontainer* | head -1)
loginfo() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] INFO: $1" >> $LOG_FILE
}

logrc() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] ERROR: $1" >> $LOG_FILE
}

#Create Users function
add_user() {
  loginfo "Adding user $1..."
	useradd -r -g db2iadm1 -m -d /home/$1 $1
	usermod --shell /bin/bash $1
	loginfo "User $1 created!"
}

#Create instance
instance_create() {
        loginfo "Creating instance $1..."
        /opt/ibm/db2/$VERSION/instance/db2icrt -u db2fenc1 $1
        wait
	if [ ! $(sudo -u $HADR_USER1 -i  bash -c  "db2ilist | grep -q $HADR_USER1") ]
	then
   		#Logowanie
    loginfo "Instance $1 created!"
	else
    logrc "Instance $HADR_USER1 not exists!"
		exit 0
	fi
}


#create database on PRIMARY
database_create() {
	sudo -u $1 -i  bash -c 'db2start'
	wait
  loginfo "Database $2 creating..."
	sudo -u $1 -i bash -c "db2 'create db $2'"
	wait
	if [ ! $(sudo -u $HADR_USER1 -i  bash -c "db2 list db directory | grep -q $DB_NAME") ]
        then
          loginfo "Database $DB_NAME created!"
        else
                logrc "Database $DB_NAME not exisits!"
                exit 0
        fi

	sudo -u $1 -i bash -c "db2 'UPDATE DB CFG FOR $DB_NAME USING LOGINDEXBUILD ON LOGARCHMETH1 LOGRETAIN'"
	if [ ! $(sudo -u $HADR_USER1 -i  bash -c  "db2 get db cfg for $DB_NAME | grep -q LOGRETAIN") ]
        then
          loginfo "LOGRETAIN set"
        else
                logrc "Db cfg LOGRETAIN not set!"
                exit 0
        fi

}

#hadr primary backup
backup_primary () {
  loginfo "Starting Primary database backup..."
	sudo -u $1 -i bash -c "mkdir /home/$1/backup"
	sudo -u $1 -i bash -c "db2 'BACKUP DB $DB_NAME TO /home/$1/backup'"
	wait
	if [ ! $(ls -lt /home/$1/backup/ | grep -q $DB_NAME) ]
        then
        	mv /home/$1/backup/$DB_NAME* /opt/db2_setup/hadr/
          loginfo "Primary database backup complete"
          touch /opt/db2_setup/hadr/PRIMARY_BACKUP
        else
                logrc "Primary backup not exists!"
                exit 0
        fi
}
#IP_sharing

ip_sharing(){
 grep $HOSTNAME_LOCAL  /etc/hosts >> /opt/db2_setup/hadr/ip_primary.out
}

#Grab IP from Standby
standby_ip_gather(){
	cat  /opt/db2_setup/hadr/ip_standby.out >> /etc/hosts
 	HOSTNAME_REMOTE=$(cat /opt/db2_setup/hadr/host_standby.out)
	HOSTNAME_REMOTE_IP=$(grep $HOSTNAME_REMOTE /etc/hosts | awk '{print $1}')
}
#Waiting for standby init
standby_init() {
	while [ ! -e $SC ]
	do
		printf "\n\nWaiting for Standby init...\n"
		sleep 5
	done
}

#Standby configuration
database_config_primary(){
        sudo -u $1 -i bash -c "db2 'UPDATE DB CFG FOR $DB_NAME USING HADR_LOCAL_HOST $HOSTNAME_LOCAL_IP HADR_LOCAL_SVC $PORT_PRIM HADR_SYNCMODE SYNC HADR_REMOTE_HOST $HOSTNAME_REMOTE_IP HADR_REMOTE_SVC $PORT_STAND HADR_REMOTE_INST $HADR_USER2'"
	wait
	if [ ! $(sudo -u $1 -i bash -c "db2 get db cfg for $DB_NAME | grep -q $HADR_USER2") ]
        then
        	  loginfo "Database full config set"
		        touch $PC
	else
                logrc "Database full config not set!"
                exit 0
        fi
}

#Waiting for standby init
standby_start_state() {
        while [ ! -e $SSTARTED ]
        do
                printf "\n\nWaiting for Standby Start...\n"
                sleep 5
        done
}


database_start_primary(){

        loginfo "HADR starting on Primary..."
        sudo -u $HADR_USER1 -i bash -c "db2start"
        wait
        sudo -u $HADR_USER1 -i bash -c "db2 start hadr on db $DB_NAME as primary"
        wait
				if [ ! $(sudo -u $1 -i bash -c "db2pd -db $DB_NAME -hadr | grep -q CONNECTED") ]
        then
          loginfo "HADR Primary started"
        else
                logrc "HADR Primary not started!"
                exit 0
        fi
}


envoirnment_cleanup(){

rm /opt/db2_setup/hadr/*
loginfo "Cleanup done"

}
add_user $HADR_USER1
instance_create $HADR_USER1
database_create $HADR_USER1 $DB_NAME
backup_primary $HADR_USER1

#Primary hostname
echo $HOSTNAME > '/opt/db2_setup/hadr/host_primary.out'
ip_sharing

standby_init
standby_ip_gather
database_config_primary $HADR_USER1
standby_start_state
database_start_primary
envoirnment_cleanup
