#!/bin/bash

#Variables
HADR_USER1="db2hadrp"
HADR_USER2="db2hadrs"
DB_NAME="HLDB"
HOSTNAME_SET=$(hostname)
PORT_PRIM=50100
PORT_STAND=50101

#Logging
TIMESTAMP=$(date +%F_%T)
LOG_FILE=$(ls -t /opt/db2_setup/logs/db2multicontainer* | head -1)
loginfo() {
	printf "\n$TIMESTAMP [$HOSTNAME_SET][$0] INFO: $1" >> $LOG_FILE
}

logrc() {
	printf "\n$TIMESTAMP [$HOSTNAME_SET][$0] ERROR: $1" >> $LOG_FILE
}

#Create Users function
add_user() {
  loginfo "Adding user $1..."
	useradd -r -g db2iadm1 -m -d /home/$1 $1
	usermod --shell /bin/bash $1
  loginfo "User $1 added"
}

#Create instance
instance_create() {
        loginfo "Creating instance $1..."
        /opt/ibm/db2/$VERSION/instance/db2icrt -u db2fenc1 $1
        wait
        if [ ! $(sudo -u $HADR_USER1 -i  bash -c  "db2ilist | grep -q $1") ]
        then
          loginfo "Instance $1 created!"
        else
          logrc "Instance $1 not exists!"
          exit 0
        fi
      }


#create database on PRIMARY
database_create() {
	sudo -u $1 -i  bash -c 'db2start'
	wait
  loginfo "Database $2 creating..."
	sudo -u $1 -i bash -c "db2 'create db $2'"
	wait
  if [ ! $(sudo -u $1 -i  bash -c "db2 list db directory | grep -q $2") ]
  then
    loginfo "Database $2 created!"
  else
    logrc "Database $2 not exisits!"
    exit 0
  fi
}

#Adding database config for HADR PRI
database_config_primary () {
  loginfo "Setting Primary database configuration..."
	sudo -u $1 -i bash -c "db2 'UPDATE DB CFG FOR $DB_NAME USING LOGINDEXBUILD ON LOGARCHMETH1 LOGRETAIN'"
	wait
  if [ ! $(sudo -u $HADR_USER1 -i  bash -c  "db2 get db cfg for $DB_NAME | grep -q LOGRETAIN") ]
  then
    loginfo "LOGRETAIN set"
  else
    logrc "Db cfg LOGRETAIN not set!"
    exit 0
  fi

	sudo -u $1 -i bash -c "db2 'UPDATE DB CFG FOR $DB_NAME USING HADR_LOCAL_HOST $HOSTNAME_SET HADR_LOCAL_SVC $PORT_PRIM HADR_SYNCMODE SYNC HADR_REMOTE_HOST $HOSTNAME_SET HADR_REMOTE_SVC $PORT_STAND HADR_REMOTE_INST $HADR_USER2'"
	wait
  if [ ! $(sudo -u $1 -i bash -c "db2 get db cfg for $DB_NAME | grep -q $HADR_USER2") ]
  then
    loginfo "Database full config set"
	else
    logrc "Database full config not set!"
    exit 0
  fi

	sudo -u $1 -i bash -c "mkdir /home/$HADR_USER1/backup"
	printf "\nStarting Primary database backup...\n"
	sudo -u $1 -i bash -c "db2 'BACKUP DB $DB_NAME TO /home/$HADR_USER1/backup'"
	wait
  if [ ! $(ls -lt /home/$HADR_USER1/backup/ | grep -q $DB_NAME) ]
  then
    loginfo "Primary database backup complete"
  else
    logrc "Primary backup not exists!"
    exit 0
  fi
}

#Adding database config for HADR STB
database_config_standby(){
  loginfo "Setting Standby database configuration..."
  sudo -u $1 -i  bash -c "mkdir /home/$HADR_USER2/backup"
  mv /home/$HADR_USER1/backup/$DB_NAME* /home/$HADR_USER2/backup
  chown $HADR_USER2:db2iadm1 /home/$HADR_USER2/backup/$DB_NAME*
  chmod 777  /home/$HADR_USER2/backup/$DB_NAME*
	sudo -u $1 -i  bash -c "db2start"
	wait

  loginfo "Restoring database on Standby"
	sudo -u $1 -i  bash -c "db2 restore db $DB_NAME from /home/$HADR_USER2/backup/ on /home/$HADR_USER2"
  wait
  if [ ! $(sudo -u $1 -i  bash -c "db2 list db directory | grep -q $DB_NAME") ]
  then
    loginfo "Standby database Restored"
  else
    logrc "Standby database not exists! Restoration failed!"
    exit 0
  fi

  loginfo "Standaby full database configuration..."
  sudo -u $1 -i bash -c "db2 'UPDATE DB CFG FOR $DB_NAME USING HADR_LOCAL_HOST $HOSTNAME_SET HADR_LOCAL_SVC $PORT_STAND HADR_SYNCMODE SYNC HADR_REMOTE_HOST $HOSTNAME_SET HADR_REMOTE_SVC $PORT_PRIM HADR_REMOTE_INST $HADR_USER1'"
  if [ ! $(sudo -u $1 -i bash -c "db2 get db cfg for $DB_NAME | grep -q $HADR_USER1") ]
  then
    loginfo "Standby full database config complete"
  else
    logrc "HADR_REMOTE_INST wrong parameter!"
    exit 0
  fi

}

#Starting Database on PRIMARY and STANDBY
database_start_hadr(){

  loginfo "Starting HADR on Standby..."
	sudo -u $HADR_USER2 -i bash -c "db2start"
	wait
	sudo -u $HADR_USER2 -i bash -c "db2 start hadr on db $DB_NAME as standby"
	wait
	sudo -u $HADR_USER1 -i bash -c "db2start"
	wait

  loginfo "Starting HADR on Primary..."
	sudo -u $HADR_USER1 -i bash -c "db2 start hadr on db $DB_NAME as primary"
	wait
  if [ ! $(sudo -u $HADR_USER1 -i bash -c "db2pd -db $DB_NAME -hadr | grep -q CONNECTED") ]
  then
    loginfo "HADR Primary started"
  else
    logrc "HADR Primary not started!"
    exit 0
  fi
}

#Adding HADR user 1
add_user $HADR_USER1
#Adding HADR user 2
add_user $HADR_USER2

#Creating instance Primary
instance_create $HADR_USER1
#Creating instance Standby
instance_create $HADR_USER2

#creating database
database_create $HADR_USER1 $DB_NAME

#Configuration for primary database
database_config_primary $HADR_USER1

#Configuration for standby database
database_config_standby $HADR_USER2

#Starting database on STANDBY and PRIMARY
database_start_hadr
