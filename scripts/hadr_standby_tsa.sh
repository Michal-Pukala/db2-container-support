#!/bin/bash

#Variables
HADR_USER1="db2hadrp"
HADR_USER2="db2hadrs"
DB_NAME="HLDB"
HOSTNAME_LOCAL=$(hostname)
HOSTNAME_LOCAL_IP=$(grep $HOSTNAME_LOCAL /etc/hosts | awk '{print $1}')
HOSTNAME_REMOTE=$(cat /opt/db2_setup/hadr/host_primary.out)
PORT_PRIM=50100
PORT_STAND=50101
PB="/opt/db2_setup/hadr/PRIMARY_BACKUP"
SC="/opt/db2_setup/hadr/STANDBY_CONFIGURED"
PC="/opt/db2_setup/hadr/PRIMARY_CONFIGURED"
SS="/opt/db2_setup/hadr/STANDBY_STARTED"
SSTARTED="/opt/db2_setup/hadr/STANDBY_STARTED_START"
TIMESTAMP=$(date +%F_%T)
IP_QUORUM=$(route | awk '/default/ { print $2 }')

#Logging
LOG_FILE=$(ls -t /opt/db2_setup/logs/db2multicontainer* | head -1)
loginfo() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] INFO: $1" >> $LOG_FILE
}

logrc() {
	printf "\n$TIMESTAMP [$HOSTNAME_LOCAL][$0] ERROR: $1" >> $LOG_FILE
}

#Primary check state
primary_state() {

loginfo "Standby waiting for Primary backup..."
while [ ! -e $PB ]
do
	sleep 5
done
}

#Create Users function
add_user() {
  loginfo "Adding user $1..."
	useradd -r -g db2iadm1 -m -d /home/$1 $1
	usermod --shell /bin/bash $1
	loginfo "User $1 added"
}

#Create instance
instance_create() {
	loginfo "Creating instance $1..."
  /opt/ibm/db2/$VERSION/instance/db2icrt -u db2fenc1 $1
  wait
  if [ ! $(sudo -u $HADR_USER2 -i  bash -c  "db2ilist | grep -q $HADR_USER2") ]
		then
 			loginfo "Instance $HADR_USER2 created"
		else
 			logrc "Instance $HADR_USER2 not exists!"
			exit 0
	fi
}

#Adding database config for HADR STB
database_restore_standby(){
	loginfo "Setting Standby database configuration..."
  sudo -u $1 -i  bash -c "mkdir /home/$HADR_USER2/backup"
  mv /opt/db2_setup/hadr/$DB_NAME* /home/$HADR_USER2/backup
  chown $HADR_USER2:db2iadm1 /home/$HADR_USER2/backup/$DB_NAME*
  chmod 777  /home/$HADR_USER2/backup/$DB_NAME*
	sudo -u $1 -i  bash -c "db2start"
	wait
	loginfo "Restoring database on Standby..."
	sudo -u $1 -i  bash -c "db2 restore db $DB_NAME from /home/$HADR_USER2/backup/ on /home/$HADR_USER2"
  wait
	if [ ! $(sudo -u $HADR_USER2 -i  bash -c "db2 list db directory | grep -q $DB_NAME") ]
  	then
    	loginfo "Standby database Restored"
    else
    	logrc "Standby database not exists! Restoration failed!"
      exit 0
  fi
}

#Standby configuration
database_config_standby(){
	sudo -u $1 -i bash -c "db2 'UPDATE DB CFG FOR $DB_NAME USING HADR_LOCAL_HOST $HOSTNAME_LOCAL_IP HADR_LOCAL_SVC $PORT_STAND HADR_SYNCMODE SYNC HADR_REMOTE_HOST $HOSTNAME_REMOTE_IP HADR_REMOTE_SVC $PORT_PRIM HADR_REMOTE_INST $HADR_USER1 hadr_peer_window 120'"
  if [ ! $(sudo -u $1 -i bash -c "db2 get db cfg for $DB_NAME | grep -q $HADR_USER1") ]
  	then
    	loginfo "Standby full database config complete"
			touch $SC
		else
			logrc "HADR_REMOTE_INST wrong parameter!"
      exit 0
  fi
}

#Standby waiting for Primary config
primary_config_state() {
loginfo "Standby waiting for PRIMARY configuration..."
while [ ! -e $PC ]
	do
  	sleep 5
	done
}

#IP_sharing
ip_sharing(){
	grep $HOSTNAME_LOCAL  /etc/hosts >> /opt/db2_setup/hadr/ip_standby.out
}

#Grab IP from Primary
primary_ip_gather(){
	cat  /opt/db2_setup/hadr/ip_primary.out >> /etc/hosts
	HOSTNAME_REMOTE=$(cat /opt/db2_setup/hadr/host_primary.out)
  HOSTNAME_REMOTE_IP=$(grep $HOSTNAME_REMOTE /etc/hosts | awk '{print $1}')
}

database_start_standby(){
	loginfo "HADR starting on Standby..."
	sudo -u $HADR_USER2 -i bash -c "db2start"
	wait
	sudo -u $HADR_USER2 -i bash -c "db2 start hadr on db $DB_NAME as standby"
	wait
	touch $SSTARTED
}

setup_envoirnment(){
	loginfo "Starting TSA setup on Standby"
	#enable watchdog process
	#Remove after image container update
	sudo apt-get update
	wait
	sudo apt-get install linux-modules-5.4.0-126-generic
	wait
	sudo apt-get install linux-modules-5.4.0-128-generic
	wait
	##
	modprobe softdog
	#install SAM packages process
	if [ ! $(sudo -u root -i bash -c "$(find /opt/db2_setup/setup_package/ -name 'installSAM') --noliccheck --silent -l /tmp/installSAM.log") ]
  	then
    	loginfo "SAM installed succesfully "
		else
			logrc "installSAM failed"
      exit 0
  fi

	#install HADR monitor scripts
	$(find /opt -name 'db2cptsa')
	if [ !$(find /opt -name 'db2cptsa' | grep -q DBI1110I) ]
  	then
    	loginfo "TSA scripts installed "
    else
      logrc "TSA scripts install failed"
      exit 0
  fi

	export CT_MANAGEMENT_SCOPE=2
	#need stop process srcmsrt
	srcmstrctrl -k
	systemctl start srcmstr
	systemctl start ctrmc
	systemctl enable ctrmc

	systemctl enable srcmstr
	if [ !$(systemctl status ctrmc | grep -q running) ]
  	then
    	loginfo "ctrmc service started "
    else
    	logrc "ctrmc service disabled"
      exit 0
  fi

	preprpnode $HOSTNAME_REMOTE $HOSTNAME_LOCAL
	wait

	if [ !$(grep -q "$HOSTNAME_LOCAL" /var/ct/cfg/ctrmc.acls) ] && [ !$(grep -q "$HOSTNAME_REMOTE" /var/ct/cfg/ctrmc.acls) ]
  	then
    	loginfo "preprnode executed succesfull "
    else
    	logrc "preprnode failed"
      exit 0
  fi
}

db2haicu_setup(){
	HOSTNAME_REMOTE_IP=$(grep $HOSTNAME_REMOTE /etc/hosts | awk '{print $1}')
echo "
<!-- =============================================-->
<!-- =           DB2 Domain Information         = -->
<!-- =   Quorum Device, Network Interface and   = -->
<!-- =          Cluster Node Details            = -->
<!-- =============================================-->

<DB2Cluster xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"db2ha.xsd\" version=\"1.0\" clusterManagerName=\"TSA\">
<ClusterDomain domainName=\"hahadr\">
<Quorum quorumDeviceProtocol=\"network\" quorumDeviceName=\"$IP_QUORUM\"/>
<PhysicalNetwork physicalNetworkName=\"db2_private_network_0\" physicalNetworkProtocol=\"ip\">
<Interface interfaceName=\"eth0\" clusterNodeName=\"$HOSTNAME_LOCAL\">
<IPAddress baseAddress=\"$HOSTNAME_LOCAL_IP\" subnetMask=\"255.255.0.0\" networkName=\"db2_private_network_0\"/>
</Interface>
<Interface interfaceName=\"eth0\" clusterNodeName=\"$HOSTNAME_REMOTE\">
<IPAddress baseAddress=\"$HOSTNAME_REMOTE_IP\" subnetMask=\"255.255.0.0\" networkName=\"db2_private_network_0\"/>
</Interface>
</PhysicalNetwork>
<ClusterNode clusterNodeName=\"$HOSTNAME_REMOTE\"/>
<ClusterNode clusterNodeName=\"$HOSTNAME_LOCAL\"/>
</ClusterDomain>

<!-- =============================================-->
<!-- =             Failover Policy              = -->
<!-- =============================================-->

<FailoverPolicy>
<HADRFailover/>
</FailoverPolicy>

<!-- =============================================-->
<!-- =             DB Partition Set             = -->
<!-- =============================================-->

<DB2PartitionSet>
<DB2Partition dbpartitionnum=\"0\" instanceName=\"db2hadrs\"/>
</DB2PartitionSet>

<!-- =============================================-->
<!-- =          HADR Database Details           = -->
<!-- =============================================-->

<HADRDBSet>
<HADRDB databaseName=\"HLDB\" localInstance=\"db2hadrs\" remoteInstance=\"db2hadrp\" localHost=\"$HOSTNAME_LOCAL\" remoteHost=\"$HOSTNAME_REMOTE\" monitorDataMounts=\"false\"/>
</HADRDBSet>
</DB2Cluster>
" >> /opt/db2_setup/hadr/haicu_standby.xml
 sudo -u $HADR_USER2 -i bash -c "db2haicu -f /opt/db2_setup/hadr/haicu_standby.xml"
 wait
	if [ !$(lssam | grep -q IBM) ]
 		then
    	touch /opt/db2_setup/hadr/tsa_standby_setup_done
			loginfo "db2haicu installed on Standby"
    else
	  	logrc "db2haicu installation failed"
      exit 0
    fi
}

#Standby hostname
echo $HOSTNAME > '/opt/db2_setup/hadr/host_standby.out'
ip_sharing
primary_state
primary_ip_gather
setup_envoirnment
add_user $HADR_USER2
instance_create $HADR_USER2
database_restore_standby $HADR_USER2
database_config_standby $HADR_USER2
primary_config_state
database_start_standby
db2haicu_setup
