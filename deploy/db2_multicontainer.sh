#!/bin/bash

deploy()
{
	case $1 in

		hadr_remote)
			sudo mkdir -p /tmp/db2contout/logs &&  sudo mkdir -p /tmp/db2contout/hadr && sudo touch /tmp/db2contout/logs/db2multicontainer_$(date +%F_%T).log
			sudo docker build -t db2multic ..
			wait
			sudo docker run -itd --privileged=true --net=bridge -v /tmp/db2contout/hadr:/opt/db2_setup/hadr -v /tmp/db2contout/logs:/opt/db2_setup/logs/ -e HADR_PRIMARY=YES --name=hadr_multi_p db2multic
			sudo docker exec -td hadr_multi_p /opt/db2_setup/scripts/setup_db2.sh
			sudo docker run -itd --privileged=true --net=bridge -v /tmp/db2contout/hadr:/opt/db2_setup/hadr -v /tmp/db2contout/logs:/opt/db2_setup/logs/ -e HADR_STANDBY=YES --name=hadr_multi_s db2multic
			sudo docker exec -td hadr_multi_s /opt/db2_setup/scripts/setup_db2.sh
			;;
		tsa_remote)
			sudo mkdir -p /tmp/db2contout/logs &&  sudo mkdir -p /tmp/db2contout/hadr && sudo touch /tmp/db2contout/logs/db2multicontainer_$(date +%F_%T).log
			sudo docker build -t db2multic ..
			wait
			sudo docker run -itd --privileged=true --net=bridge -v /tmp/db2contout/hadr:/opt/db2_setup/hadr -v /tmp/db2contout/logs:/opt/db2_setup/logs/ -e HADR_PRIMARY_TSA=YES --name=tsa_multi_p db2multic
			sudo docker exec -td tsa_multi_p /opt/db2_setup/scripts/setup_db2.sh
			sudo docker run -itd --privileged=true --net=bridge -v /tmp/db2contout/hadr:/opt/db2_setup/hadr -v /tmp/db2contout/logs:/opt/db2_setup/logs/ -e HADR_STANDBY_TSA=YES --name=tsa_multi_s db2multic
			sudo docker exec -td tsa_multi_s /opt/db2_setup/scripts/setup_db2.sh
			;;
		hadr_local)
			sudo mkdir -p /tmp/db2contout/logs &&  sudo mkdir -p /tmp/db2contout/hadr && sudo touch /tmp/db2contout/logs/db2multicontainer_$(date +%F_%T).log
			sudo docker build -t db2multic ..
			wait
			sudo docker run -itd --privileged=true --net=host -v /tmp/db2contout/logs:/opt/db2_setup/logs -e HADR_LOCAL=YES --name=hadr_local db2multic
			sudo docker exec -td hadr_local /opt/db2_setup/scripts/setup_db2.sh
			;;
		default_instance)
			sudo mkdir -p /tmp/db2contout/logs && sudo touch /tmp/db2contout/logs/db2multicontainer_$(date +%F_%T).log
			sudo docker build -t db2multic ..
			wait
			sudo docker run -itd --privileged=true --net=host -v /tmp/db2contout/logs:/opt/db2_setup/logs -e DEFAULT_INSTANCE=YES --name=db2_default db2multic
			sudo docker exec -td db2_default /opt/db2_setup/scripts/setup_db2.sh
			;;
		*)
			echo "Wrong parameter"
	esac
}

deploy $1
